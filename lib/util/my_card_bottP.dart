import 'package:flutter/material.dart';

class MyCardBottP extends StatelessWidget {
  const MyCardBottP({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        width: 350,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 245, 247, 245),
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            
             const Text(
              'Plastic Bottle',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
             const SizedBox(
              height: 40,
            ),
                Image.asset('lib/images/bottlePl.png',
                width: 150,
                height: 200,),
            const SizedBox(
              height: 40,
            ),
             const Text(
              '1 per = 12 point',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
