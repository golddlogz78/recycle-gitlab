class Money {
  final int id;
  final String point, title, description, images;

  Money({
    required this.id, 
    required this.point, 
    required this.title, 
    required this.description, 
    required this.images});
}

// list Exchage Money
List<Money> moneys = [
  Money(
    id: 1, 
    point: "500 Point", 
    title: "200 Baht.", 
    description: "Exchange 500 point for 200 baht", 
    images: "lib/images/coin2.png"
    ),

    Money(
    id: 2, 
    point: "1000 Point", 
    title: "500 Baht.", 
    description: "Exchange 1000 point for 500 baht", 
    images: "lib/images/coin2.png"
    ),

    Money(
    id: 3, 
    point: "1400 Point", 
    title: "700 Baht.", 
    description: "Exchange 1400 point for 700 baht", 
    images: "lib/images/coin2.png"
    ),

    Money(
    id: 4, 
    point: "1900 Point", 
    title: "1,000 Baht.", 
    description: "Exchange 1900 point for 1,000 baht", 
    images: "lib/images/coin2.png"
    ),
];