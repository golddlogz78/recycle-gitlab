import 'package:flutter/material.dart';

class AboutSett extends StatelessWidget {
  const AboutSett({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        width: 450,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 193, 229, 193),
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text(
              '" Recycle for a better world: Earn rewards, save the planet! "',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "This is an innovative application that aims to contribute to the preservation of our planet's environment. It enables users to manage waste effectively by promoting recycling of materials that can be reused to create various useful items. By participating in this program, users can earn points, which can be redeemed for a range of benefits, including discounts, cashback, and other incentives. In this way, we hope to encourage people to adopt more sustainable habits and help protect the environment for future generations.",
              style: TextStyle(
                color: Color.fromARGB(255, 7, 7, 7),
                fontSize: 17,
                //fontWeight: FontWeight.bold
              ),
            ),
          ],
        ),
      ),
    );
  }
}
