class Discount2 {
  final int id;
  final String point, title, images;

  Discount2({
    required this.id, 
    required this.point, 
    required this.title, 
    required this.images});
}

// list Exchage Money
List<Discount2> discounts = [
  Discount2(
    id: 1, 
    point: "50 Point", 
    title: "20% Discount for shampoo", 
    images: "lib/images/disdis.png"
    ),

    Discount2(
    id: 2, 
    point: "50 Point", 
    title: "20% Discount for soap", 
    images: "lib/images/disdis.png"
    ),

    Discount2(
    id: 3, 
    point: "200 Point", 
    title: "15% for per bill", 
    images: "lib/images/disdis.png"
    ),

   Discount2(
    id: 4, 
    point: "250 Point", 
    title: "20% for per bill", 
    images: "lib/images/disco.png"
    ),
];