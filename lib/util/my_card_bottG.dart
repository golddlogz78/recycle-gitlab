import 'package:flutter/material.dart';

class MyCardBottG extends StatelessWidget {
  const MyCardBottG({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        width: 350,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 245, 247, 245),
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            
             const Text(
              'Glass Bottle',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
             const SizedBox(
              height: 10,
            ),
                Image.asset('lib/images/bottleG.png',
                width: 230,
                height: 235,),
            const SizedBox(
              height: 40,
            ),
             const Text(
              '1 per = 20 point',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
