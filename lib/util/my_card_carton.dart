import 'package:flutter/material.dart';

class MyCardCart extends StatelessWidget {
  const MyCardCart({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        width: 350,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 245, 247, 245),
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            
             const Text(
              'Carton',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
             const SizedBox(
              height: 50,
            ),
                Image.asset('lib/images/milk.png',
                width: 200,
                height: 200,),
            const SizedBox(
              height: 40,
            ),
             const Text(
              '1 per = 5 point',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
