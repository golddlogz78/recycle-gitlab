import 'package:flutter/material.dart';

class MyCardCan extends StatelessWidget {
  const MyCardCan({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        width: 350,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 245, 247, 245),
            borderRadius: BorderRadius.circular(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            
             const Text(
              'Can',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
             const SizedBox(
              height: 55,
            ),
                Image.asset('lib/images/can2.png',
                width: 250,
                height: 150,),
            const SizedBox(
              height: 80,
            ),
             const Text(
              '1 per = 7 point',
              style: TextStyle(
                  color: Color.fromARGB(255, 7, 7, 7),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
