class Ticket {
  final int id;
  final String point, title, images;

  Ticket({
    required this.id, 
    required this.point, 
    required this.title, 
    required this.images});
}

// list Exchage Money
List<Ticket> tickets = [
  Ticket(
    id: 1, 
    point: "100 Point", 
    title: "50% Discount for drink", 
    images: "lib/images/ticket2.png"
    ),

    Ticket(
    id: 2, 
    point: "350 Point", 
    title: "50% Discount for ticket", 
    images: "lib/images/ticket2.png"
    ),

    Ticket(
    id: 3, 
    point: "500 Point", 
    title: "Buy 1 get 1 popcorn",  
    images: "lib/images/ticket2.png"
    ),

    Ticket(
    id: 4, 
    point: "1000 Point", 
    title: "Free Tickic",  
    images: "lib/images/ticket2.png"
    ),
];