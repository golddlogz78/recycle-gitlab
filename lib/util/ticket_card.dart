import 'package:flutter/material.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/util/money_list.dart';
import 'package:flutter_application_1/util/ticket_list.dart';

class TicketCard extends StatelessWidget {
  const TicketCard({
    Key? key,
    required this.itemIndex,
    required this.ticket,
  }) : super(key: key);

  final int itemIndex;
  final Ticket ticket;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: kDefaultPadding,
        vertical: kDefaultPadding / 2,
      ),
      height: 160,
      child: Stack(
        children: <Widget>[
          Container(
            height: 136,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              color: Colors.green,
            ),
            child: Container(
              margin: const EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(22),
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
              height: 130,
              width: 160,
              child: Image.asset(
                ticket.images,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            bottom: -5,
            left: 0,
            child: SizedBox(
              height: 136,
              width: size.width - 250,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                    child: Text(
                      ticket.title.toString(),
                      style: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: kDefaultPadding * 1,
                      vertical: kDefaultPadding / 2,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.amberAccent,
                      borderRadius: BorderRadius.all(Radius.circular(22)),
                    ),
                    child: Text(
                      ticket.point,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
