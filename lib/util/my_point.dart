import 'package:flutter/material.dart';

class MyPoint extends StatelessWidget {
  const MyPoint({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: SingleChildScrollView(
        child: SafeArea(
            child: Center(
              child: Column(
                      children: [
            
              const SizedBox(height: 15),
            
              //card my point
              Container(
                width: 385,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 245, 247, 245),
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Column(children: const [
                  Text(
                    'My Point',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 7, 7, 7),
                      fontSize: 25,
                    ),
                  ),
                  Text(' '),
                  Text(
                    '1,000,000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 11, 179, 42),
                      fontSize: 30,
                    ),
                  ),
                ]),
              ),
                      ],
                    ),
            )),
      ),
    );
  }
}