import 'package:flutter/material.dart';

import 'package:flutter_application_1/components/my_textfield.dart';
import 'package:flutter_application_1/firebase_auth_methods.dart';
import 'package:flutter_application_1/pages/login_page.dart';
import 'package:provider/provider.dart';

import '../components/my_button2.dart';

class EmailPasswordSignup extends StatefulWidget {
  static String routeName = '/signup-email-password';
  const EmailPasswordSignup({Key? key}) : super(key: key);

  @override
  State<EmailPasswordSignup> createState() => _EmailPasswordSignupState();
}

class _EmailPasswordSignupState extends State<EmailPasswordSignup> {
  // text editing controllers

  final TextEditingController passwordController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  @override
  void dispose() {
    super.dispose();

    emailController.dispose();
    passwordController.dispose();
  }

  // sign user in method
  void signUpUser() async {
    await context.read<FirebaseAuthMethods>().signUpWithEmail(
          email: emailController.text,
          password: passwordController.text,
          context: context,
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 30, 106, 48),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 20),

              // Sign in
              const Text(
                'Sign Up',
                style: TextStyle(
                  color: Color.fromARGB(255, 255, 252, 252),
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                ),
              ),

              const SizedBox(height: 20),

              // password textfield
              MyTextField(
                prefixIcon: const Icon(
                  Icons.mail,
                  color: Color.fromARGB(255, 84, 125, 69),
                ),
                controller: emailController,
                hintText: 'Email',
                obscureText: false,
              ),

              const SizedBox(height: 10),

              // password textfield
              MyTextField(
                prefixIcon: const Icon(
                  Icons.lock,
                  color: Color.fromARGB(255, 84, 125, 69),
                ),
                controller: passwordController,
                hintText: 'Password',
                obscureText: true,
              ),

              const SizedBox(height: 30),

              // sign in button
              MyButtonSignUp(
                // onTap: signUserIn,
                onTap: signUpUser,
              ),

              const SizedBox(height: 10),

              // not a member? Sign up now
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    'Have an account?',
                    style: TextStyle(color: Color.fromARGB(255, 46, 44, 44)),
                  ),
                  SizedBox(width: 4),
                  SignUpBtn(),
                ],
              )

              // or continue with
              // const Padding(
              //   padding: EdgeInsets.symmetric(horizontal: 130),
              //   child: Row(
              //     children: [
              //        Text(
              //             '\nHave an account?',
              //             style:
              //                 TextStyle(color: Color.fromARGB(255, 46, 44, 44)),
              //           ),
              //           SizedBox(width: 4),
              //           SignUpBtn(),
              // not a member? Sign up now
              // Column(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Text(
              //       '\nHave an account?',
              //       style:
              //           TextStyle(color: Color.fromARGB(255, 46, 44, 44)),
              //     ),
              //     SignUpBtn(),
              //   ],
              // )
              //],
              //),
              //),
            ],
          ),
        ),
      ),
    );
  }
}

class SignUpBtn extends StatelessWidget {
  const SignUpBtn({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: const Text(
        'Sign in now',
        style: TextStyle(
          color: Color.fromARGB(255, 82, 208, 95),
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
