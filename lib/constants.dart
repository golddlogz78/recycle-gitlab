import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromARGB(255, 135, 226, 102);
const kTextColor = Color.fromARGB(255, 1, 1, 1);
const kBackgroundColor = Color.fromARGB(255, 30, 106, 48);

const double kDefaultPadding = 20.0;
