import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/components/home_screen.dart';
import 'package:flutter_application_1/util/about.dart';
import 'package:flutter_application_1/screens/components/condition.dart';
import 'package:flutter_application_1/util/help.dart';
import 'package:flutter_application_1/util/language.dart';
import 'package:flutter_application_1/util/report.dart';

class SettPage extends StatelessWidget {
  const SettPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        title: const Text("About Recycle Plant"),
        elevation: 0,
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: const [
              SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 30,
              ),
              AboutSett(),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
