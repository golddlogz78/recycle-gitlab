import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/components/payment_page.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'home_screen.dart';

class ScanPageNew extends StatefulWidget {
  const ScanPageNew({Key? key}) : super(key: key);

  @override
  State<ScanPageNew> createState() => _SCState();
}

class _SCState extends State<ScanPageNew> {
  bool _flashOn = false;
  bool _frontCam = false;
  GlobalKey _qrKey = GlobalKey();
  late QRViewController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(
          Icons.image,
          size: 35,
        ),
        backgroundColor: const Color.fromARGB(255, 100, 145, 73),
      ),
      //backgroundColor: bgColor,
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        title: const Text("QR Scan"),
        elevation: 0,
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        actions: [
          IconButton(
            icon: Icon(_flashOn ? Icons.flash_on : Icons.flash_off),
            iconSize: 25.0,
            onPressed: () {
              setState(() {
                _flashOn = !_flashOn;
              });
              _controller.toggleFlash();
            },
            alignment: Alignment.centerLeft,
          ),
        ],
      ),

      body: Stack(
        children: <Widget>[
          QRView(
              key: _qrKey,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.white,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: MediaQuery.of(context).size.width * 0.8,
              ),
              onQRViewCreated: (QRViewController controller) {
                this._controller = controller;
              }),
        ],
      ),
    );
  }
}
