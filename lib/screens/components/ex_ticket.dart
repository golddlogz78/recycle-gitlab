import 'package:flutter/material.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/reward_page/homepage2.dart';
import 'package:flutter_application_1/screens/components/home_screen.dart';
import 'package:flutter_application_1/util/my_point.dart';
import 'package:flutter_application_1/util/ticket_card.dart';
import 'package:flutter_application_1/util/ticket_list.dart';

class ExcTicket extends StatelessWidget {
  const ExcTicket({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ticket"),
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        elevation: 0,
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
      ),
      body: SafeArea(
          bottom: false,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Expanded(
                    child: Container(
                  width: 385,
                  height: 130,
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 245, 247, 245),
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(children: const [
                    Text(
                      'My Point',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 7, 7, 7),
                        fontSize: 25,
                      ),
                    ),
                    Text(' '),
                    Text(
                      '1,000,000',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 11, 179, 42),
                        fontSize: 30,
                      ),
                    ),
                  ]),
                )),
              ),
              const SizedBox(
                height: 25,
              ),
              Expanded(
                  child: Stack(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 70),
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 220, 243, 203),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40))),
                  ),
                  ListView.builder(
                      // here we use our demo procuts list
                      itemCount: tickets.length,
                      itemBuilder: (context, index) => TicketCard(
                            itemIndex: index,
                            ticket: tickets[index],
                          ))
                ],
              ))
            ],
          )),
    );
  }
}
