import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/components/home_screen.dart';
import 'package:flutter_application_1/screens/components/profile_page.dart';
import 'package:flutter_application_1/screens/components/setting_page.dart';

class NullPage extends StatelessWidget {
  const NullPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        title: const Text("Coming Soon"),
        elevation: 0,
        leading: BackButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const SettPage(),
              ),
            );
          },
          color: Colors.white,
        ),
      ),
      body: Center(
        child: Column(
          children: const [
            SizedBox(
              height: 200,
            ),
            Text(
              'Thank U!!!',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50,
                  color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
