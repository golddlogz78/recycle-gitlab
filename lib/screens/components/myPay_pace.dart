import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_application_1/components/my_button_save.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/screens/components/input_formatters.dart';
import 'package:flutter_application_1/screens/components/profile_page.dart';

class MyPay extends StatefulWidget {
  const MyPay({
    super.key,
  });

  @override
  State<MyPay> createState() => _MyPayState();
}

class _MyPayState extends State<MyPay> {
  //CardType cardType = CardType.Invail
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 30, 106, 48),
      appBar: AppBar(
        title: const Text("My Payment"),
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Column(
            children: [
              const SizedBox(
                height: 25,
              ),
              Form(
                  child: Column(
                children: [
                  TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(16),
                      CardNumberInputFormatter()
                    ],
                    decoration: const InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Card Number",
                        prefixIcon: Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Icon(
                            Icons.credit_card,
                            color: Color.fromARGB(255, 30, 106, 48),
                          ),
                        ),
                        floatingLabelAlignment: FloatingLabelAlignment.start,
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 0, 0, 0)))),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    decoration: const InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Fullname",
                        prefixIcon: Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Icon(
                            Icons.person,
                            color: Color.fromARGB(255, 30, 106, 48),
                          ),
                        ),
                        floatingLabelAlignment: FloatingLabelAlignment.start,
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                          child: TextFormField(
                        keyboardType: TextInputType.number,
                        // Limit the input
                        inputFormatters: [
                          //Now
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(3),
                          CardNumberInputFormatter()
                        ],
                        decoration: const InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            hintText: "CVV",
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Icon(
                                Icons.heart_broken,
                                color: Color.fromARGB(255, 30, 106, 48),
                              ),
                            ),
                            floatingLabelAlignment:
                                FloatingLabelAlignment.start,
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25))),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                borderSide: BorderSide(color: Colors.black))),
                      )),
                      const SizedBox(width: kDefaultPadding),
                      Expanded(
                          child: TextFormField(
                        keyboardType: TextInputType.number,
                        // Limit the input
                        inputFormatters: [
                          //Now
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(4)
                        ],
                        decoration: const InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            hintText: "MM/YY",
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Icon(
                                Icons.calendar_month,
                                color: Color.fromARGB(255, 30, 106, 48),
                              ),
                            ),
                            floatingLabelAlignment:
                                FloatingLabelAlignment.start,
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25))),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                borderSide: BorderSide(color: Colors.black))),
                      )),
                    ],
                  ),
                ],
              )),
              Padding(
                padding: const EdgeInsets.only(top: kDefaultPadding),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 30,
                    ),
                    MyButtonSave(onTap: () {
                      Navigator.pop(context);
                    })
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
