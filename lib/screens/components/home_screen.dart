import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/components/profile_page.dart';
import 'package:flutter_application_1/screens/components/scan_pate_new.dart';
import 'package:flutter_application_1/screens/components/setting_page.dart';
import 'package:flutter_application_1/util/my_card_bottG.dart';
import 'package:flutter_application_1/util/my_card_bottP.dart';
import 'package:flutter_application_1/util/my_card_bottPu.dart';
import 'package:flutter_application_1/util/my_card_can.dart';
import 'package:flutter_application_1/util/my_card_carton.dart';
import 'package:flutter_application_1/reward_page/homepage2.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../firebase_auth_methods.dart';

class HomePage extends StatefulWidget {
  static String routeName = '/home';
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //pagecontroller
  final _controller = PageController();
  @override
  Widget build(BuildContext context) {
    final user = context.read<FirebaseAuthMethods>().user;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const ScanPageNew(),
            ),
          );
        },
        backgroundColor: const Color.fromARGB(255, 100, 145, 73),
        child: const Icon(
          Icons.qr_code_scanner,
          size: 33,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        elevation: 0,
        color: const Color.fromARGB(255, 100, 145, 73),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.home_rounded),
              iconSize: 40,
              color: const Color.fromARGB(255, 245, 247, 245),
            ),
            IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage2(),
                  ),
                );
              },
              icon: const Icon(Icons.savings_rounded),
              iconSize: 40,
              color: const Color.fromARGB(255, 245, 247, 245),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
          children: [
            //app bar
            AppBar(
              backgroundColor: const Color.fromARGB(255, 30, 106, 48),
              title: const Text("Recycle Plant"),
              elevation: 0,
              actions: [
                IconButton(
                  icon: const Icon(Icons.favorite),
                  iconSize: 40.0,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const SettPage(),
                        ));
                  },
                  alignment: Alignment.centerLeft,
                ),
              ],
              leading: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const ProfilePage(),
                          ),
                        );
                      },
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 3, vertical: 1),
                        child: CircleAvatar(
                          radius: 25.0,
                          backgroundColor:
                              const Color.fromARGB(255, 121, 201, 75),
                          child: SizedBox(
                            width: 45,
                            height: 45,
                            child: ClipOval(
                              child: Image.network(
                                user.photoURL ?? '',
                                width: 100,
                                height: 100,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            const SizedBox(height: 25),

            //card my point
            Container(
              width: 385,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 245, 247, 245),
                borderRadius: BorderRadius.circular(16),
              ),
              child: Column(children: const [
                Text(
                  'My Point',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 7, 7, 7),
                    fontSize: 25,
                  ),
                ),
                Text(' '),
                Text(
                  '1,000,000',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 11, 179, 42),
                    fontSize: 30,
                  ),
                ),
              ]),
            ),

            const SizedBox(
              height: 25,
            ),
            //card price
            SizedBox(
              height: 420,
              child: PageView(
                scrollDirection: Axis.horizontal,
                controller: _controller,
                children: const [
                  MyCardBottP(),
                  MyCardBottG(),
                  MyCardBottPu(),
                  MyCardCan(),
                  MyCardCart(),
                ],
              ),
            ),

            const SizedBox(
              height: 20,
            ),
            SmoothPageIndicator(
              controller: _controller,
              count: 5,
              effect: const ScrollingDotsEffect(
                  activeDotColor: Color.fromARGB(255, 117, 184, 77)),
            )
          ],
        )),
      ),
    );
  }
}
