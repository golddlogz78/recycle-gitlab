import 'package:flutter/material.dart';
import 'package:flutter_application_1/util/logout.dart';
import 'package:flutter_application_1/util/my_add.dart';
import 'package:flutter_application_1/util/my_pay.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

import '../../firebase_auth_methods.dart';

class ProfilePage extends StatelessWidget {
  static String routeName = ('/profile');
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    final user = context.read<FirebaseAuthMethods>().user;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: const Color.fromARGB(255, 30, 106, 48),
            title: const Text("Profile"),
            elevation: 0,
            actions: [
              IconButton(
                icon: const Icon(Icons.edit),
                iconSize: 25.0,
                onPressed: () {},
                alignment: Alignment.centerLeft,
              ),
            ]),
        body: Column(
          children: <Widget>[
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        margin: const EdgeInsets.only(top: 2),
                        child: Stack(children: [
                          CircleAvatar(
                            radius: 120,
                            backgroundColor:
                                const Color.fromARGB(255, 121, 201, 75),
                            child: SizedBox(
                              width: 95,
                              height: 95,
                              child: ClipOval(
                                child: Image.network(
                                  user.photoURL ?? '',
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ),
                      const SizedBox(
                        height: 25,
                      ),

                      Text(
                        user.displayName ?? "",
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: Colors.white),
                      ),

                      const SizedBox(
                        height: 5,
                      ),

                      Text(
                        user.providerData[0].email ?? "",
                        style:
                            const TextStyle(fontSize: 17, color: Colors.white),
                      ),

                      const SizedBox(
                        height: 25,
                      ),
                      //card price
                      SizedBox(
                        height: 350,
                        child: Column(
                          children: const [
                            SizedBox(
                              height: 30,
                            ),
                            MyPayment(),
                            SizedBox(
                              height: 30,
                            ),
                            MyAdd(),
                          ],
                        ),
                      ),

                      const SizedBox(
                        height: 20,
                      ),
                      Logout(
                        onTap: () {
                          context.read<FirebaseAuthMethods>().signOut(context);
                          GoogleSignIn().disconnect();
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
