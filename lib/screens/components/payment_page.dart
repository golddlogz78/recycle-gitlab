import 'package:flutter/material.dart';
import 'package:flutter_application_1/components/my_button.dart';
import 'package:flutter_application_1/components/my_button2.dart';
import 'package:flutter_application_1/components/my_button_save.dart';
import 'package:flutter_application_1/components/my_textfield.dart';
import 'package:flutter_application_1/components/square_tile.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/pages/login_page.dart';
import 'package:flutter_application_1/screens/components/home_screen.dart';
import 'package:flutter_application_1/screens/components/profile_page.dart';

class TestPage extends StatefulWidget {
  TestPage({super.key});

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  int value = 0;
  final paymentLabels = [
    'Credit card / Debit card',
    'True Money wallet',
  ];

  final paymentIcons = [
    Icons.credit_card,
    Icons.account_balance_wallet,
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Payment"),
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        elevation: 0,
        leading: BackButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const HomePage(),
              ),
            );
          },
          color: Colors.white,
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.edit),
            iconSize: 25.0,
            onPressed: () {},
            alignment: Alignment.centerLeft,
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [                
          SizedBox(height: 10,),
          Expanded(
            child: ListView.separated(
              itemCount: paymentLabels.length,
              itemBuilder: (context, index) {
                return TextButton(
                  style: TextButton.styleFrom(
                    fixedSize: Size(1, 75),                    
                  ),
                  onPressed: () { },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      // leading: Radio(
                      //   activeColor: Colors.white,
                      //   value: index,
                      //   groupValue: value,
                      //   onChanged: (i) => setState(() => value = i!),
                      // ),
                      title: Text(
                        paymentLabels[index],
                        style: const TextStyle(color: Colors.white),
                      ),
                      trailing: Icon(
                        paymentIcons[index],
                        color: Colors.white,
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return const Divider();
              },
            ),
          ),

        ],
        
        ),
      ),
      
    );
  }
}
