// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/components/ex_ticket.dart';

class MyTicket extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: Padding(       
        padding: const EdgeInsets.all(20.0),
        child:Material(
          borderRadius: BorderRadius.circular(20),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: InkWell(
            onTap: () {
               Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ExcTicket()));
            },
            child: Ink.image(
              image: AssetImage('lib/images/ticket.png'),
               fit: BoxFit.cover,
            ),
          ),
        ),

        
      ),
    );
  }
}
