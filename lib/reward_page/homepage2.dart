// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:flutter_application_1/reward_page/categorie/money.dart';
import 'package:flutter_application_1/reward_page/categorie/discount.dart';
import 'package:flutter_application_1/reward_page/categorie/ticket.dart';
import 'package:flutter_application_1/screens/components/home_screen.dart';

class HomePage2 extends StatelessWidget {
  static String routeName = ('/homepage2');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 30, 106, 48),
        centerTitle: true,
        title: const Text('Exchange Categories'),
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
      ),
      body: ListView(
        children: [
          Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Ex Money
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Money',
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              MyMoney(),

              // const Divider(
              //   thickness: 0.5,
              //   color: Colors.black,
              //   indent: 20,
              //   endIndent: 20,
              // ),

              //Ex Discount
              const SizedBox(
                height: 1,
              ),
              const Text(
                'Discount',
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              MyDiscount(),
              // const Divider(
              //   thickness: 0.5,
              //   color: Colors.black,
              //   indent: 20,
              //   endIndent: 20,
              // ),

              //Ex Ticket
              const SizedBox(
                height: 1,
              ),
              const Text(
                'Ticket',
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              MyTicket(),
              // const Divider(
              //   thickness: 0.5,
              //   color: Colors.black,
              //   indent: 20,
              //   endIndent: 20,
              // ),

              // //Ex Furniture
              // const SizedBox(
              //   height: 2,
              // ),
              // const Text(
              //   'Furniture',
              //   style: TextStyle(
              //       fontSize: 23,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.white),
              // ),
              // MyFurni(),
            ],
          )),
        ],
      ),
    );
  }
}
